#This Script Will... 
#end correct and subtract quadratic :Done:
#----------------------------------------------------------------------------------------------------------------------
#Imports 

import numpy as np
import glob
import pandas as pd
import scipy
import matplotlib.pyplot as disp
import warnings  
import xlsxwriter
from scipy import signal

warnings.simplefilter("ignore")

#matplotlib.style.use('ggplot')
#data = pd.ExcelFile('Final_analysis.xlsx')
#Output = ['IronFoilCalc', 'Fe2O3Calc', 'Fe3O4 Calc']
#tabnames = data.sheet_names

#---------------------------------------------------------------------------------------------------------------------
###Part 1 :Working:

#Pull Data From Excel And Then Pull From DF
book = pd.ExcelFile("Final_analysis.xlsx")
iron_foil_sheet = book.parse("IronFoilCalc")
iron_ox_sheet = book.parse("Fe2O3Calc")
iron_foil_df = iron_foil_sheet[["channel","value"]]
iron_ox_df = iron_ox_sheet[["channel","value"]]


foil_quad = iron_foil_sheet[['quad']]
ox_quad = iron_ox_sheet[['quad']]

#Debug testing part 1 
#May Remove Check Plots, Leave For Now
#Check Plot
#x = iron_foil_df['channel']
#y = iron_foil_df['value']
#disp.scatter(x,y)
#disp.plot(x,y)
#disp.show()
#print(iron_ox_df)
#print(foil_quad)

#-------------------------------------------------------------------------------------------------------------------
###Part 2 :Working:

#Subtract quad data now to make it easier 
iron_foil_df['value'] = iron_foil_df['value'] - foil_quad['quad']
iron_ox_df['value'] = iron_ox_df['value'] - ox_quad['quad']

#iron_foil_df['value'] - foil_quad['quad']
#iron_ox_df['value'] - ox_quad['quad']



#Debug for part 2
#Check Plot
#x1 = foil_quad_sub['channel']
#y1 = foil_quad_sub['value']
#disp.figure(figsize=(30,30))
#disp.scatter(x1,y1)
#disp.show()
#print(iron_foil_df)
#print(iron_ox_df)
#print(iron_foil_df)

#--------------------------------------------------------------------------------------------------------------------
###Part 3

#Clean Up Ends, end points look ok may change later as static vars are bad practice...
holder1 = iron_foil_df.drop(iron_foil_df.index[:325])
holder2 = iron_ox_df.drop(iron_ox_df.index[:1468])
iron_foil_end_cor = holder1.drop(holder1.index[-325:])
iron_ox_end_cor = holder2.drop(holder2.index[-1468:])

#iron_foil_end_cor = iron_foil_df.query('-300 <= value <= 300')
#iron_ox_end_cor = iron_ox_df.query('-300 <= value <= 300')

#Debug for part 3 
#Test plot 
#x1 = iron_foil_end_cor['channel']
#y1 = iron_foil_end_cor['value']
#disp.figure(figsize=(30,30))
#disp.scatter(x1,y1)
#disp.show()
#print(iron_ox_end_cor)

#-------------------------------------------------------------------------------------------------------------------
###Part 4 make new xlsx book with cleaned data

#Make Book File
#newBook = pd.ExcelWriter('Cleaned_data.xlsx', engine = "xlsxwriter" )

#print(iron_ox_write)
#Dataframes to sheets
#iron_foil_end_cor.to_excel(newBook, sheet_name='Iron_Foil')
#iron_ox_end_cor.to_excel(newBook, sheet_name='Iron_Ox')

#Save pandas book
#newBook.save()
#newBook.close()
#-------------------------------------------------------------------------------------------------------------------
#Text output that script is complete 

print('\n' * 3)
print("***Scipt is finished, cleaned data saved in xlsx file 'Cleaned_Data'***")
print('\n' * 3)



#-------------------------------------------------------------------------------------------------------------------------
##Section 5 data graph 

#Peak ID Script 

iron_foil_df = iron_foil_end_cor
iron_ox_df = iron_ox_end_cor

#---------------------------------------------------------------------------------------------

#Smooth + Find Peaks 
###Smooth Foil 
x_foil = iron_foil_df['channel']
y_foil = iron_foil_df['value']
ysmth_foil = signal.savgol_filter(iron_foil_df['value'], 67, 3) #Value, Size, order
array_y_foil = ysmth_foil
peaks_foil, _ = signal.find_peaks(-array_y_foil, height=75,distance=100 )
peaks_foil_df = pd.DataFrame( peaks_foil + 5535, array_y_foil[peaks_foil])


###Smooth Oxide 
x_ox = iron_ox_df['channel']
y_ox = iron_ox_df['value']
ysmth_ox = signal.savgol_filter(iron_ox_df['value'], 99 ,3) #Value, Size, order
ymsth = ysmth_ox[(ysmth_ox >= -300) & (ysmth_ox <= 300)] 
array_y_ox = ysmth_ox
peaks_ox, _ = signal.find_peaks(-array_y_ox, height=70 ,distance=100 )
peaks_ox_df = pd.DataFrame(peaks_ox + 5440, array_y_ox[peaks_ox])

#Debug
#Gen Test Plot
#disp.plot(x_foil,y_foil)
disp.plot(x_foil,ysmth_foil, color='red')
#disp.plot(x_ox,ysmth_ox, color='red')
disp.plot(peaks_foil + 5275 , array_y_foil[peaks_foil], "x")
#disp.plot(peaks_ox + 5490 , array_y_ox[peaks_ox], "x")
disp.show()
#print(peaks_foil_df)
#print(peaks_ox_df)













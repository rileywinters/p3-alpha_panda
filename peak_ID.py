#Peak ID Script 
#ID Peaks in data 

#Comments 

#-------------------------------------------------------------------------------------------
#Imports 

import scipy.signal as signal  
import warnings 
import pandas as pd
import matplotlib.pyplot as disp
import numpy as np

#--------------------------------------------------------------------------------------
###Section 1 Load data from cleaned data 

book = pd.ExcelFile("Cleaned_data.xlsx" )
iron_foil_sheet = book.parse("Iron_Foil")
iron_ox_sheet = book.parse("Iron_Ox")
iron_foil_df = iron_foil_sheet[['channel','value']]
iron_ox_df = iron_ox_sheet[['channel','value']]

value_foil = iron_foil_df['value']
value_ox = iron_ox_df['value']

#Debug/Test Section 
#x1 = iron_foil_df['channel']
#y1 = iron_foil_df['value']
#disp.figure(figsize=(30,30))
#disp.plot(x1,y1)
#disp.show()

#---------------------------------------------------------------------------------------------
#Section 2 Smooth and Peak ID

#Smooth + Find Peaks 
###Smooth Foil 
x_foil = iron_foil_df['channel']
y_foil = iron_foil_df['value']
ysmth_foil = signal.savgol_filter(iron_foil_df['value'], 67, 3) #Value, Size, order
array_y_foil = ysmth_foil
peaks_foil, _ = signal.find_peaks(-array_y_foil, height=75,distance=100 )
peaks_foil_df = pd.DataFrame( peaks_foil + 5535, array_y_foil[peaks_foil])


###Smooth Oxide 
x_ox = iron_ox_df['channel']
y_ox = iron_ox_df['value']
ysmth_ox = signal.savgol_filter(iron_ox_df['value'], 99 ,3) #Value, Size, order
ymsth = ysmth_ox[(ysmth_ox >= -300) & (ysmth_ox <= 300)] 
array_y_ox = ysmth_ox
peaks_ox, _ = signal.find_peaks(-array_y_ox, height=70 ,distance=100 )
peaks_ox_df = pd.DataFrame(peaks_ox + 5490, array_y_ox[peaks_ox])

#Debug
#Gen Test Plot
#disp.plot(x,y)
#disp.plot(x_foil,ysmth_foil, color='red')
#disp.plot(x_ox,ysmth_ox, color='red')
#disp.plot(peaks_foil + 5535 , array_y_foil[peaks_foil], "x")
#disp.plot(peaks_ox + 5490 , array_y_ox[peaks_ox], "x")
#disp.show()
#print(peaks_foil_df)
#print(peaks_ox_df)


#--------------------------------------------------------------------------------------------------------
#Section 3 Fit 










